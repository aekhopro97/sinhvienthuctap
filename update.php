<!DOCTYPE html>
<html>
<head>
	<?php include('conn.php'); ?>
	<meta http-equiv="content-type" content="text/html: charset=utf-8 "/>
	<title>Form Update</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<form class="form-horizontal" action="updatesv.php" method="POST">
<fieldset>
	<span style="font-size:25px; color:blue"><center><strong>Thông Tin Sinh Viên</strong></center></span> 
	<div class="mid" style="margin:auto; padding:auto; width:80%;">
<div class="form-group"> 
	<label class="col-md-4 control-label" for="Masv">Mã sv</label> 
	<div class="col-md-4">
	<input id="Masv" name="Masv" type="text" placeholder="Nhập mã sv" class="form-control input-md" required=""
	 value="<?php echo($_GET['Masv']);?>">
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label" for="Tensv">Tên sv</label>  
	<div class="col-md-4">
	<input id="Tensv" name="Tensv" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo($_GET['Tensv']);?>">
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label" for="Lop">Lớp</label>  
	<div class="col-md-4">
	<input id="Lop" name="Lop" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo($_GET['Lop']);?> ">  
  	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label" for="Diachi">Địa Chỉ</label>  
	<div class="col-md-4">
	<input id="Diachi" name="Diachi" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo($_GET['Diachi']);?>">
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label" for="txtsdt">SĐT</label>  
	<div class="col-md-4">
	<input id="SDT" name="SDT" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo($_GET['SDT']);?>">
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label" for="Ngaysinh">Ngày sinh</label>  
	<div class="col-md-4">
	<input id="Ngaysinh" name="Ngaysinh" type="text" placeholder="" class="form-control input-md" required="" value="<?php echo($_GET['Ngaysinh']);?>"> 
	</div>
</div>
<div class="form-group">
	<label class="col-md-4 control-label" for="btnluu"></label>
	<div class="col-md-8">
    <button id="btnluu" name="btnluu" class="btn btn-success">Save</button>
	</div>
</div>
</div>
</fieldset>
</form>
</body>
</html>